from sys import argv, exit

script, current_file=argv

# Opening and reading line
txt=open(current_file).readline()
print "Date to parse", txt

# To convert list into string
string = ''.join(str(e) for e in txt)

# Function to extract required letters out of whole string
def input_inclusive_first_and_last_index(inclusive_first_index, inclusive_last_index):
    count_character = inclusive_first_index - 1
    date = string[count_character]
    while count_character<inclusive_last_index:
        if count_character >= inclusive_first_index and count_character < inclusive_last_index:
            date += string[count_character]
        count_character += 1
    return date

month_names = ['January', 'February', 'March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

def month_prediction(month):
    return month_names[int(month) - 1]

def print_value(format_string, inclusive_first_index, inclusive_last_index):
    print format_string % input_inclusive_first_and_last_index(inclusive_first_index, inclusive_last_index)

def print_month(string):
    month = month_prediction(input_inclusive_first_and_last_index(6, 7))
    print string, month   

print "\nAfter parsing"
print_value("Year is %s", 1, 4)
print_month("Month is")
print_value("Date of month is %s", 9, 10)
print_value("Hour is %s", 12, 13)
print_value("Minutes are %s", 15, 16)
print_value("Seconds are %s", 18, 19)
