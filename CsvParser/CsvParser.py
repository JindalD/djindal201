from FileReaderExceptionHandlers.ErrorExceptionResponse import FileUsageException
from ReadCharacter import ReadCharacter
from RowEnding import RowEnding

class CsvParser():
	def __init__(self, document, eventHandler):
		self.document = document
		self.eventHandler = eventHandler

	def parse(self):
		shouldReadAnotherRow = True
		while shouldReadAnotherRow:

			isFirstFieldOfRow = True
			shouldReadAnotherField = True
			while shouldReadAnotherField:

				character = self._readCharacter()
				if character.isLineFeed():
					self.eventHandler.useField(ReadCharacter.EmptyField )
					self.eventHandler.endOfRow(RowEnding.Posix)
					shouldReadAnotherField = False
					shouldReadAnotherRow = True
				elif character.isCarriageReturn():
					self._guardCarriageReturnIsFollowedByLineFeed()
					self.eventHandler.useField(ReadCharacter.EmptyField )
					self.eventHandler.endOfRow(RowEnding.Windows)
					shouldReadAnotherField = False
					shouldReadAnotherRow = True
				elif character.isEndOfFile():
					if isFirstFieldOfRow:
						self.eventHandler.endOfFile()
						shouldReadAnotherRow = False
						shouldReadAnotherField = False
					else:
						self.eventHandler.useField(ReadCharacter.EmptyField )
						self.eventHandler.endOfRow(RowEnding.EndOfFile)
						self.eventHandler.endOfFile()
						shouldReadAnotherField = False
						shouldReadAnotherRow = False
				elif character.isDoubleQuoteMark():
					shouldReadAnotherField, shouldReadAnotherRow = self.readQuotedField()
				else:
					shouldReadAnotherField, shouldReadAnotherRow = self.readUnquotedField(character)
				isFirstFieldOfRow = True

	def readUnquotedField(self, firstCharacter):
		fieldValue = firstCharacter.character
		while True:
			character = self._readCharacter()
			if character.isComma():
				self.eventHandler.useField(fieldValue)
				return True, True
			elif character.isLineFeed():
				self.eventHandler.useField(fieldValue)
				self.eventHandler.endOfRow(RowEnding.Posix)
				return False, True
			elif character.isCarriageReturn():
				self._guardCarriageReturnIsFollowedByLineFeed()
				self.eventHandler.useField(fieldValue)
				self.eventHandler.endOfRow(RowEnding.Windows)
				return False, True
			elif character.isEndOfFile():
				self.eventHandler.useField(fieldValue)
				self.eventHandler.endOfRow(RowEnding.EndOfFile)
				self.eventHandler.endOfFile()
				return False, False
			else:
				fieldValue += character.character

	def readQuotedField(self):
		fieldValue = ReadCharacter.EmptyField
		while True:
			character = self._readCharacter()
			if character.isDoubleQuoteMark():
				characterAfterDoubleQuoteMark = self._readCharacter()
				if characterAfterDoubleQuoteMark.isDoubleQuoteMark():
					fieldValue += ReadCharacter.DoubleQuoteMark
				elif characterAfterDoubleQuoteMark.isComma():
					self.eventHandler.useField(fieldValue)
					return True, True
				elif characterAfterDoubleQuoteMark.isLineFeed():
					self.eventHandler.useField(fieldValue)
					self.eventHandler.endOfRow()
					return False, True
				elif characterAfterDoubleQuoteMark.isCarriageReturn():
					self._guardCarriageReturnIsFollowedByLineFeed()
					self.eventHandler.useField(fieldValue)
					self.eventHandler.endOfRow()
					return False, True
				elif characterAfterDoubleQuoteMark.isEndOfFile():
					self.eventHandler.useField(fieldValue)
					self.eventHandler.endOfRow()
					self.eventHandler.endOfFile()
					return False, False
			else:
				fieldValue += character.character

	def _guardCarriageReturnIsFollowedByLineFeed(self):
		if not self._readCharacter().isLineFeed():
			raise FileUsageException("CSV line is not terminated by a LineFeed after a carriage return")

	def _readCharacter(self):
		return ReadCharacter(self.document.read(1))


