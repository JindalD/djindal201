from CsvParser import CsvParser
from EventHandlers.ValidatingEventHandlers import ValidatingEventHandler

class CsvParsingFileUser():

	def __init__(self, eventHandler):
		self.eventHandler = eventHandler

	def useFile(self, document):
		csvParser = CsvParser(document, ValidatingEventHandler(self.eventHandler))
		csvParser.parse()