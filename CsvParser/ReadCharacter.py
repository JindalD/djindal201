class ReadCharacter():
	Comma = ","
	DoubleQuoteMark = '"'
	LineFeed = '\n'
	CarriageReturn = '\r'
	Tab = '\t'
	EmptyField = ""

	def __init__(self, character):
		self.character = character	

	def isComma(self):
		return self._isCharacter(self.Comma)

	def isDoubleQuoteMark(self):
		return self._isCharacter(self.DoubleQuoteMark)

	def isLineFeed(self):
		return self._isCharacter(self.LineFeed)

	def isCarriageReturn(self):
		return self._isCharacter(self.CarriageReturn)

	def isEndOfFile(self):
		return self.character == self.EmptyField

	def _isCharacter(self, like):
		return self.character == like