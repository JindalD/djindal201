from sys import argv
from CsvParsingFileUser import CsvParsingFileUser
from EventHandlers.PrintingEventHandler import PrintingEventHandler
from EventHandlers.TwoDimensionalArrayBuildingEventHandler import TwoDimensionalArrayBuildingEventHandler
from EventHandlers.PeopleEventHandler import PeopleEventHandler
from EventHandlers.MultiplyDispatchingEventHandler import MultiplyDispatchingEventHandler
from FileReaderExceptionHandlers.FileReader import fileReader

def main():
	script, csvFileName = argv
	printingEventHandler = PrintingEventHandler()
	twoDimensionalArrayBuildingEventHandler = TwoDimensionalArrayBuildingEventHandler()
	peopleEventHandler = PeopleEventHandler()

	EventHandler = MultiplyDispatchingEventHandler(printingEventHandler, twoDimensionalArrayBuildingEventHandler, peopleEventHandler)
	csvParserFileUser = CsvParsingFileUser(EventHandler)
	fileReader(csvFileName, csvParserFileUser)

	print twoDimensionalArrayBuildingEventHandler.twoDimensionalArray
	print peopleEventHandler.people

main()