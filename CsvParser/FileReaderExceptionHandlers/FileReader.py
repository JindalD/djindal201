from sys import exc_info
from FileReaderExceptionHandlers.ErrorExceptionResponse import IOErrorResponse
from FileReaderExceptionHandlers.ErrorExceptionResponse import FileUsageException
from FileReaderExceptionHandlers.ErrorExceptionResponse import UnexpectedErrorResponse
from FileReaderExceptionHandlers.ErrorExceptionResponse import FileUsegesExceptionResponse

def fileReader(filePath, fileUser):
    document = OpeningFile(filePath)
    try:
        document
    except IOError, exception:
        IOErrorResponse(filePath, exception.errno, exception.strerror)
    try:
        fileUser.useFile(document)
    # handle exceptions from fileUser
    except FileUsageException, exception:
        FileUsegesExceptionResponse(filePath, exception.message)

    except IOError, exception:
        IOErrorResponse(filePath, exception.errno, exception.strerror)
    except:
        exceptionType, value, traceback = exc_info()
        UnexpectedErrorResponse(filePath, exceptionType, value)
        raise
    finally:
        document.close()

def OpeningFile(filePath):
    return open(filePath)

