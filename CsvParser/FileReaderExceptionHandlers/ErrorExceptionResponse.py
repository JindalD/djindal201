from WriteToStandardError import writeToStandardErrorAndExit
from WriteToStandardError import writeToStandardError

def IOErrorResponse(filePath, errno, exception_error):
    writeToStandardErrorAndExit("Could not open/read file '%s' because of error number '%d' with message '%s'",
                                filePath, errno, exception_error)

def FileUsegesExceptionResponse(filePath, message):
    writeToStandardErrorAndExit("Could not use file '%s' because '%s'", filePath, message)


def UnexpectedErrorResponse(filePath, exceptionType, value):
    writeToStandardError("Unexpected exception using file '%s' with type '%s' with value '%s'", filePath, exceptionType,
                         value)

class FileUsageException(Exception):
    def __init__(self, messageFormatString, *arguments):
        self.message = messageFormatString % arguments



