from sys import exit
from sys import stderr
from os import linesep

FailureExitCode = 1

def writeToStandardErrorAndExit(messageFormatString, filePath, *arguments):
    writeToStandardError(messageFormatString, filePath, *arguments)
    exit(FailureExitCode)

def writeToStandardError(messageFormatString, filePath, *arguments):
    formatArguments = (filePath,) + arguments
    stderr.write((messageFormatString + linesep) % formatArguments)