from FileReaderExceptionHandlers.ErrorExceptionResponse import FileUsageException
from RowEnding import RowEnding

class ValidatingEventHandler():

	def __init__(self, eventHandler):
		self.eventHandler = eventHandler
		self.headerFieldCount = 1
		self.rowCount = 1
		self._resetFieldCount()
		self.rowEnding = RowEnding.NotYetKnown

	def useField(self, fieldValue):
		if self._isParsingHeaderRow():
			self.headerFieldCount += 1
			print self.headerFieldCount
		else:
			self.fieldCount += 1
			print self.fieldCount
			if self.fieldCount > self.headerFieldCount:
				raise FileUsageException("The header row contained '%d' field(s), but this row (%d; one-based) contains more fields", self.headerFieldCount, self.rowCount)
		self.eventHandler.useField(fieldValue)

	def endOfRow(self, rowEnding):
		if self._isParsingHeaderRow():
			self.rowEnding = rowEnding
		else:
			if self.fieldCount < self.headerFieldCount:
				raise FileUsageException("The header row contained '%d' field(s), but this row (%d; one-based) contains only '%d' field(s)", self.headerFieldCount, self.rowCount, self.fieldCount)
			if self.rowEnding != rowEnding:
				raise FileUsageException("A body row (%d; one-based) had a different line ending to the header row", self.rowCount)
			self._resetFieldCount()
		self.rowCount += 1
		self.eventHandler.endOfRow(rowEnding)

	def endOfFile(self):
		self.eventHandler.endOfFile()

	def _isParsingHeaderRow(self):
		return self.rowCount == 1

	def _resetFieldCount(self):
		self.fieldCount = 1