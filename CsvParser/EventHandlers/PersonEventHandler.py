class Person():

	def __init__(self, eyeColour, hairColour, height, dateOfBirth):
		self.eyeColour = eyeColour
		self.hairColour = hairColour
		self.height = height
		self.dateOfBirth = dateOfBirth

	def __repr__(self):
		return "Person(%s, %s, %s, %s)" % (self.eyeColour, self.hairColour, self.height, self.dateOfBirth)