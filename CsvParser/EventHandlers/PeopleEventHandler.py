from PersonEventHandler import Person

class PeopleEventHandler():

	def __init__(self):
		self.people = []
		self._newCurrentRow()

	def useField(self, fieldValue):
		self.currentRow.append(fieldValue)

	def endOfRow(self, rowEnding):
		self.people.append(Person(*self.currentRow))
		self._newCurrentRow()

	def endOfFile(self):
		pass

	def _newCurrentRow(self):
		self.currentRow = []