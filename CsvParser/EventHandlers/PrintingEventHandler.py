class PrintingEventHandler():

	def useField(self, fieldValue):
		print "Field Value '%s'" % fieldValue

	def endOfRow(self, rowEnding):
		print "End of row"

	def endOfFile(self):
		print "End of file"