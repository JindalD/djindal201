class MultiplyDispatchingEventHandler():

	def __init__(self, *eventHandlers):
		self.eventHandlers = eventHandlers

	def useField(self, fieldValue):
		for eventHandler in self.eventHandlers:
			eventHandler.useField(fieldValue)

	def endOfRow(self, rowEnding):
		for eventHandler in self.eventHandlers:
			eventHandler.endOfRow(rowEnding)

	def endOfFile(self):
		for eventHandler in self.eventHandlers:
			eventHandler.endOfFile()