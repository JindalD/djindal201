from django.forms import fields
from applications.polls.models import Question
from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, Fieldset, Submit
from django import forms


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('question_text','pub_date',)

print Question.objects.all()

    # def __init__(self, *args, **kwargs):
    #     super(QuestionForm, self).__init__(*args, **kwargs)
    #
    #     self.helper = FormHelper()
    #     self.helper.form_class = 'form-horizontal well'
    #     self.helper.label_class = 'col-md-3'
    #     self.helper.field_class = 'col-md-6'
    #     self.helper.layout = Layout(
    #         Div(
    #             Fieldset(
    #                 '',
    #                 Field('question_text'),
    #                 Field('pub_date'),
    #             ),
    #             FormActions(
    #                 Div(
    #                     Submit('submit', 'Submit question name',
    #                            css_class='btn-u center'),
    #                     css_class='text-center'
    #                 ),
    #             ), css_class='col-md-12'
    #
    #         )
    #     )