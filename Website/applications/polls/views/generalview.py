from django.shortcuts import get_object_or_404
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse
from applications.polls.models import Question, Choice


def detail(request, question_id):
    template_name = 'polls/detail.html'
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, template_name, {'question': question})


def results(request, question_id):
    template_name = 'polls/results.html'
    question = get_object_or_404(Question, pk=question_id)
    return render(request, template_name, {'question': question})


def vote(request, question_id):
    template_name = 'polls/detail.html'
    p = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = p.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, template_name, {
            'question': p,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=(p.id,)))


def index(request):
    template_name = 'polls/index.html'
    latest_question_list = Question.objects.all()
    context = {'latest_question_list': latest_question_list}
    return render(request, template_name, context)
