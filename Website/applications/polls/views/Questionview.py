from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from applications.polls.forms.QuestionForm import QuestionForm
from applications.polls.models import Question
from django.views.generic import View


class questionview(View):

    def get(self, request, *args, **kwargs):
        template_name = 'polls/question.html'

        form = QuestionForm()
        print form

        return render(request, template_name, {'form': form})


    def post(self, request, *args, **kwargs):
        template_name = 'polls/question.html'
        form = QuestionForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('polls/question.html')
        else:
            return HttpResponseRedirect('polls')
        return render(request, template_name, {'form': form})
