from django.conf.urls import patterns, url
from applications.polls.views.Questionview import questionview
from applications.polls.views.generalview import index, detail, results, vote


urlpatterns = patterns('',
                       url(r'^$', index, name='polls_index'),
                       # url(r'^/get_profile/$', views.get_name, name='polls_profile'),
                       url(r'^(?P<question_id>\d+)/$', detail, name='detail'),
                       url(r'^(?P<question_id>\d+)/results/$', results, name='results'),
                       url(r'^(?P<question_id>\d+)/vote/$', vote, name='vote'),
                       url(r'^question/',questionview.as_view(), name='question'),

                       )
