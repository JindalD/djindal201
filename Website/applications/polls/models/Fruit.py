from django.db import models

class Food(models.Model):
    food_category = models.CharField(max_length= 20)


class Fruit(models.Model):
    food_category = models.ForeignKey(Food)
    name = models.CharField(max_length=100,help_text='These are not animals',verbose_name= 'These are the fruits',\
                            primary_key=True)

    def __unicode__(self):
        return self.name


