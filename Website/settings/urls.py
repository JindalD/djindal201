from django.conf.urls import patterns, include, url
from django.contrib import admin
from views import index

urlpatterns = patterns('',
                       url(r'^$', index, name='homepage_index') ,
                       url(r'^polls/', include('applications.polls.urls', namespace = "polls")),
                       url(r'^admin/', include(admin.site.urls)),
                       )